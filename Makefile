default: lib/libtrashmaster.a

OBJS = $(patsubst %.cpp,%.o,$(shell find src -iname '*.cpp'))

clean:
	rm -rf lib
	rm -f $(OBJS)

CFLAGS = -g -pthread -Iinclude
LIBS = -ldl

%.o: %.cpp
	g++ -c $^ -o $@ $(CFLAGS)

lib/libtrashmaster.a: $(OBJS)
	mkdir -p lib
	ar -cr $@ $^

install: lib/libtrashmaster.a
	cp lib/* /usr/lib
	mkdir -p /usr/include/trashmaster
	cp include/* /usr/include/trashmaster
