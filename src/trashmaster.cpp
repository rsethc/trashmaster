#include "trashmaster.h"
#include <cstddef>
#include <stdio.h>

void tmRealm::Collect ()
{
    std::lock_guard<decltype(graph_mutex)> graph_lock(graph_mutex);

    for (tmObj* obj : all_objs)
        obj->reached = false;

    std::vector<tmObj*> to_visit;
    for (tmObj* root_obj : root_objs)
        to_visit.push_back(root_obj);

    while (to_visit.size())
    {
        tmObj* visiting = to_visit[to_visit.size() - 1];
        to_visit.pop_back();

        visiting->reached = true;

        for (tmRef* ref : visiting->refs)
            if (!ref->target_obj->reached)
                to_visit.push_back(ref->target_obj);
    }

    for (tmObj* obj : all_objs)
    {
        if (!obj->reached && !obj->newly_created)
        {
            //printf("deleting unreachable %X\n", obj);
            delete obj;
        }
    }
}

tmObj::tmObj (tmRealm* realm)
    : realm(realm)
{
    std::lock_guard<decltype(realm->graph_mutex)> graph_lock(realm->graph_mutex);

    index_in_realm_objs = realm->all_objs.size();
    realm->all_objs.push_back(this);
}
tmObj::~tmObj ()
{
    std::lock_guard<decltype(realm->graph_mutex)> graph_lock(realm->graph_mutex);

    // Remove self from realm.
    realm->all_objs[index_in_realm_objs] = realm->all_objs[realm->all_objs.size() - 1];
    realm->all_objs[index_in_realm_objs]->index_in_realm_objs = index_in_realm_objs;
    realm->all_objs.pop_back();
}

tmRef::tmRef (tmObj* source_obj, tmObj* target_obj)
    : source_obj(source_obj)
{
    Update(target_obj);
}
void tmRef::Update (tmObj* new_target_obj)
{
    if (target_obj != new_target_obj) // Don't do anything if not changing.
    {
        std::lock_guard<decltype(source_obj->realm->graph_mutex)> graph_lock(source_obj->realm->graph_mutex);

        if (target_obj && new_target_obj)
        {
            // We don't need to add or remove this ref from refs vector.

            new_target_obj->newly_created = false;
        }
        else if (target_obj)
        {
            // We are removing the existing reference and becoming NULL.

            source_obj->refs[index_in_source_obj] = source_obj->refs[source_obj->refs.size() - 1];
            source_obj->refs[index_in_source_obj]->index_in_source_obj = index_in_source_obj;
            source_obj->refs.pop_back();
        }
        else if (new_target_obj)
        {
            // We were NULL and need to add a new reference to refs vector.

            index_in_source_obj = source_obj->refs.size();
            source_obj->refs.push_back(this);

            new_target_obj->newly_created = false;
        }

        target_obj = new_target_obj;
    }
}

tmRootObj::tmRootObj (tmRealm* realm)
    : tmObj(realm)
{
    std::lock_guard<decltype(realm->graph_mutex)> graph_lock(realm->graph_mutex);

    index_in_realm_roots = realm->root_objs.size();
    realm->root_objs.push_back(this);
}
tmRootObj::~tmRootObj ()
{
    std::lock_guard<decltype(realm->graph_mutex)> graph_lock(realm->graph_mutex);

    // Remove self from realm roots.
    realm->root_objs[index_in_realm_roots] = realm->root_objs[realm->root_objs.size() - 1];
    realm->root_objs[index_in_realm_roots]->index_in_realm_roots = index_in_realm_roots;
    realm->root_objs.pop_back();
}
