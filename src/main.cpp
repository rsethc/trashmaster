#include "trashmaster.h"
#include <stdio.h>
#include <thread>
#include <atomic>
#include <unistd.h>

tmRealm test_realm;

std::atomic_int total {0};

struct B;
struct A : public tmObj
{
    A () : tmObj(&test_realm) { total++; }
    ~A () { total--; }
    tmPtr<B> b {this};
};
struct B : public tmObj
{
    B () : tmObj(&test_realm) { total++; }
    ~B () { total--; }
    tmPtr<A> a {this};
};
struct VM : public tmRootObj
{
    VM () : tmRootObj(&test_realm) {}
    tmPtr<A> a {this};
};


void CollectorProc ()
{
    while (true)
    {
        //printf("heap objects: %d\n", (int)total);
        test_realm.Collect();
        //printf("collection done\n");
        sleep(1);
    }
}

void TesterProc ()
{
    while (true)
    {
        VM vm;
        vm.a = new A;
        vm.a->b = new B;
        vm.a->b->a = vm.a;
    }
}

int main ()
{
    std::thread collector_thread(CollectorProc);
    std::thread tester_thread(TesterProc);
    while (true)
    {
        usleep(100*1000);
        printf("heap objects: %d\n", (int)total);
    }
    tester_thread.join();
    collector_thread.join();
    return 0;
}
