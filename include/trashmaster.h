#pragma once

#include <vector>
#include <mutex>

class tmObj;
class tmRootObj;
class tmRealm
{
    friend class tmObj;
    friend class tmRootObj;
    friend class tmRef;
    std::vector<tmRootObj*> root_objs;
    std::vector<tmObj*> all_objs;
    std::recursive_mutex graph_mutex;
public:
    void Collect ();
};

class tmRef;
class tmObj
{
    friend class tmRef;
    friend class tmRealm;
protected:
    tmRealm* realm;
private:
    int index_in_realm_objs;
    bool reached,
         newly_created = true;
    std::vector<tmRef*> refs;
public:
    tmObj (tmRealm* realm);
    virtual ~tmObj ();
};

class tmRef
{
    friend class tmRealm;
    tmObj* source_obj;
    int index_in_source_obj;
protected:
    tmObj* target_obj = NULL;
    tmRef (tmObj* source_obj, tmObj* target_obj = NULL);
    void Update (tmObj* new_target_obj);
};

template <class TargetObjType>
class tmPtr : tmRef
{
public:
    tmPtr (tmObj* source_obj, TargetObjType* target_obj = NULL)
        : tmRef(source_obj, target_obj)
    {}
    TargetObjType* operator-> ()
    {
        return (TargetObjType*)target_obj;
    }
    TargetObjType& operator* ()
    {
        return *(TargetObjType*)target_obj;
    }
    TargetObjType* operator= (TargetObjType* new_target_obj)
    {
        Update(new_target_obj);
        return new_target_obj;
    }
};

class tmRootObj : public tmObj
{
    int index_in_realm_roots;
public:
    tmRootObj (tmRealm* realm);
    ~tmRootObj () override;
};
